<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>




<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Aliberate</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="..\css\estilos.css">
</head>

<body>

  <%@include file="/menu.jsp"%>
  <%@include file="/panic.jsp"%>

  <div class="container-crea">
    <div class="row">
      <div class="col" style="margin-left: 50px!important">
        <h1>Nueva Usuaria</h1>
      </div>
    </div>

    <c:url var="addUrl" value="/usuario/add-usuario" />


    <form:form action="${addUrl}" modelAttribute="usuario" id="formcrea" enctype="multipart/form-data" method="POST" acceptCharset="UTF-8">
      <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <spring:message code="lbl.nombre" text="Nombre" />
                <form:input path="nombre" class="form-control" 
                  placeholder="Introducir un Apodo"/>
                  <form:errors path="nombre" cssClass="error" />
              </div>
            </div>
            
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <spring:message code="lbl.password" text="Password" />
                <form:input path="password" type="password" class="form-control" id="passwordInput1" placeholder="Password"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <spring:message code="lbl.password" text="Repetir Password" />
                <form:input path="password" class="form-control" id="passwordInput2" placeholder="Password"/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <spring:message code="DNIInput" text="Dni"/>
                <form:input path="dni" class="form-control" id="dniInput" placeholder="00000000-x"/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <spring message code="descripcionInput" text="Descripcion"/>
                <textarea name="descripcion" style="border-radius: 8px!important"rows="5" cols="68" placeholder="Escribe aquí una breve descripción."></textarea>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <img class="img-thumbnail" src="../img/paquita.jpg" alt="../img/nofoto.png" />
        <div class="form-group">
        <br>
        <form:input path="foto" class="form-control-file" id="foto"/>
        </div>
        <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <form:button id="add-usuario">Añadir Usuario</form:button>
                  </div>
                </div>
              <div class="col-md-6">
                <div class="form-group">
                  <button type="cancel" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important">Cancelar</button>
                </div>
              </div>
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              </div>
            </div>
        </div>
       
      </div>
  </form:form>

  </div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>

</body>

</html>