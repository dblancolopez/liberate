package com.enfocat.liberate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import com.enfocat.liberate.models.User;
import com.enfocat.liberate.repositories.UserRepository;
import com.enfocat.liberate.service.SecurityService;
import com.enfocat.liberate.service.SecurityServiceImpl;
import com.enfocat.liberate.service.UserService;
import com.enfocat.liberate.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserValidator userValidator;

    @GetMapping(path = "/id")
    public @ResponseBody Optional<User> getIdParam(@RequestParam String id) {
        return userRepository.findById(Long.parseLong(id));
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/index";
    }

    @GetMapping("/edita")
    public String update(Model model, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userdet = (UserDetails) authentication.getPrincipal();
        String nom = userdet.getUsername();
        User user = userRepository.findByUsername(nom);
        model.addAttribute("user", user);
        return "edita";
    }

    @PostMapping("/edita/{id}")
    public String update(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {

            return "edita";
        }

        userRepository.save(user);

        return "index";
    }

    // @GetMapping("/edita")
    // public String edita(Model model,String modifica) {
    // if(modifica != null)
    // model.addAttribute("message", "Has modificado tu perfil correctamente");
    // return "edita";
    // }
    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "El nombre de usuario/password no son válidos.");

        if (logout != null) {
        
            return "login";
        }
        return "login";
    }

    @GetMapping({ "/index" })
    public String index(Model model) {
        return "index";
    }

    @GetMapping({ "/welcome" })
    public String welcome(Model model) {
        return "welcome";
    }

    @GetMapping({ "/" })
    public String home(Model model) {
        return "index";
    }

    @GetMapping("/perfil")
    public String user( Model model, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userdet = (UserDetails) authentication.getPrincipal();
        String nom = userdet.getUsername();
        User user = userRepository.findByUsername(nom);
        model.addAttribute("user", user);
        return "perfil";
    }

}
