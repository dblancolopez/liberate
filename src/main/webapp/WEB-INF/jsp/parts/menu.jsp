<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<base href="/">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="./resources/css/estilos.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<nav class="navbar navbar-expand-lg  navbar-dark bg-primary" style="background-color: rebeccapurple!important">
  <a class="navbar-brand" href="/index">
    <img src="${contextPath}/img/logo.png" width="60" height="60" style="border-radius: 25%!important"
      border-radius="25%" class="d-inline-block align-top" alt="logo">
  </a>
  <a class="navbar-brand" href="/">LIBERATE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navbarSupportedContent"
    aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  </div>
  <c:choose>
    <c:when test="${pageContext.request.userPrincipal.name==null}">



      <form class="form-inline">
        <div class="input-group">

          <input type="text" class="form-control" placeholder="Usuaria" aria-label="Username"
            aria-describedby="basic-addon1" style="margin-right: 3px!important">
        </div>
      </form>

      <form class="form-inline">
        <div class="input-group">

          <input type="password" class="form-control" placeholder="Contraseña" aria-label="Password"
            aria-describedby="basic-addon1" style="margin-right: 3px!important">
        </div>
      </form>
    
       <div>
         <form method="get" action="/login">
        <button  class="btn btn-success btn-outline-dark" style="background-color: violet!important" type="submit">Entrar</button>
      </form>
      
      </div>
   
    <div>
      <form method="get" action="/registration">
      <button  class="btn btn-success btn-outline-dark" style="background-color: violet!important" type="submit">Nueva</button>
    </form>
    </div>
  

  
    

       
      
    </c:when>
    <c:otherwise>
      <form class="form-inline">
        <div class="input-group">

          <input type="text" class="form-control" placeholder="${pageContext.request.userPrincipal.name}"
            aria-label="Username" aria-describedby="basic-addon1" style="margin-right: 3px!important" readonly>
        </div>
        <div>


          <button class="btn btn-success btn-outline-dark" style="background-color: violet!important" type="button"
            onclick="document.forms['logoutForm'].submit()">Logout</a></button>
          </div>
        </form>
        <div>
            <form method="get" action="/edita">
            <button  class="btn btn-success btn-outline-dark" style="background-color: violet!important" type="submit">Editar</button>
          </form>
          </div>
            
          


       

      <!--BOTON PARA DESCONECTAR-->
      <form id="logoutForm" method="POST" action="${contextPath}/logout">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
      </form>

    </c:otherwise>
  </c:choose>






</nav>