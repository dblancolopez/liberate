package com.enfocat.liberate.service;

public interface SecurityService {
    String findLoggedInUsername();
    

    void autoLogin(String username, String password);
}
