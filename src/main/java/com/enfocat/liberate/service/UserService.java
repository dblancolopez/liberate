package com.enfocat.liberate.service;

import com.enfocat.liberate.models.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
