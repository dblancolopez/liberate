package com.enfocat.liberate.repositories;

import com.enfocat.liberate.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
