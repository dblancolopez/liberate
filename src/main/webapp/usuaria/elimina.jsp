<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%


%>



<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Chat de Apoyo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">

</head>
<body>

<%@include file="/menu.jsp"%>
<%@include file="/panic.jsp"%>

    
   <div class="container-elimina">
       
            <form id="formcrea" action="#" method="POST">
        <div class="row">
            <div class="col-md-8">
            <div class="row">
                <div class="col-md-8">
                <div class="form-group">
                    <label for="dniInput">Introduce tu DNI para confirmar darse de baja</label>
                    <input name="dni" type="text" class="form-control" id="nombreInput"
                    placeholder="DNI 00000000-X">
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-success btn-outline-dark"
                    style="background-color: violet!important">Confirmar</button>
                </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                    <button type="button" class="btn btn-block btn-success btn-outline-dark"
                    style="background-color: violet!important">Cancelar</button>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <img class="img-thumbnail" src="http://localhost:8080/contactos/img/logo.png" alt="../img/nofoto.png" />
              
      </div>
        </form>

    </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


</body>
</html>

