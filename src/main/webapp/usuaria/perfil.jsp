<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%


    /*boolean datosOk;

    if ("POST".equalsIgnoreCase(request.getMethod())) {
        
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dni = request.getParameter("dni");


        datosOk=true;
        if (datosOk){
            Contacto a = new Contacto(nombre, email, ciudad, telefono);
            ContactoController.save(a);

            response.sendRedirect("/contactos/contacto/list.jsp");
            return;
        }
    }

*/
%>


<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Aliberate</title>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>

<body>

  <%@include file="/menu.jsp"%>
  <%@include file="/panic.jsp"%>

  <div class="container-edita"> 
      <div class="row">
            <div class="col-md-4">
                <img class="img-thumbnail" src="../img/paquita.jpg" alt="../img/nofoto.png" />    
                <p>Puntuación <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                <p>Rol: Paladin lvl 50</p>
                <button type="button" class="btn btn-primary"><i class="fas fa-wrench fa-2x"></i></button>                
            </div>
            
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Apodo</h1>             
                    </div>
                    <div class="col-md-12">
                        <h3>psmanagment@hotmail.com</h3>
                    </div>
                    <div class="col-md-12">
                        <h3>Descripción</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis consectetur, eos quo minima quis laudantium
                            quaerat expedita impedit odit recusandae, odio suscipit quos assumenda laborum officiis nulla provident eveniet
                            magni?</p>
                    </div>
                </div>
            </div>        
         </div>   
    </div>




  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>

</body>

</html>