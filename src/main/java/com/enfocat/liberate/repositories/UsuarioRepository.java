package com.enfocat.liberate.repositories;

import com.enfocat.liberate.models.Usuario;


import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

}
