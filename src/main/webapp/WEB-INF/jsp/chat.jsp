<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>Chat</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href=".\resources\css\chat.css">

     <script src="${contextPath}/resources/js/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/sockjs.min.js"></script>
    <script src="${contextPath}/resources/js/stomp.min.js "></script>
</head>
<body>

 <%@include file="navbar.jsp"%>
  <%@include file="panic.jsp"%>



<div class="tabla-online">
    <div class="gente">
        <div class="row">
                <img src="http://placekitten.com/300/300?image=${user.id}"  class="imagen" alt="perfil">
                <br>
                <br>
                <h3 class="text"><c:out value= "${pageContext.request.userPrincipal.name}"/></h3>
        </div>
    </div>
</div>
<form class="chat">
<div class="campo">
  <input type="hidden" value="<c:out value= "${pageContext.request.userPrincipal.name}"/>" id="userHidden"  > </input>
     <!--<textarea style="resize:none;margin-left: 2px;width:700px;height:450px"></textarea>-->
</div>
    <div class="mensaje">
        <div class="row">
        <div class="col-md-10">
            <div>
                <input style="margin-left: 10px!important width: 650px!important" name="text" type="text" class="form-control" id="textInput" placeholder="Escribe aqui...">
            </div>
            </div>
            <div class="col-md-2">
            <div>
                <button style="margin-left: -30px!important; width: 120px!important" type="button" class="btn btn-primary"><i class="far fa-paper-plane"></i></button>
            </div>
            </div>
        </div>
    </div>
</form>
<div id="mini-carousel" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="./resources/img/mini.jpg" class="d-block w-100" alt="carousel1">
    </div>
    <div class="carousel-item">
      <a href="https://interior.gencat.cat/ca/arees_dactuacio/seguretat/violencia-masclista-i-domestica/que-puc-fer-si-em-maltracta/"><img src="./resources/img/mini2.jpg" class="d-block w-100" alt="carousel2"></a>
    </div>
  </div>
</div>

 


<!--
<div class="container">
    <div class="row">-->
        <!-- Este es para los usuarios -->
        <!--<div class="col-md-4">
         <table class="table">
            <tbody>

                    <c:forEach items="${usuarios}" var="usuario">
                        <tr>
                            <td>${usuario.getId()}</td>
                            <td><c:out value="${usuario.getNombre()}" /></td>
                         
                        </tr>
                    </c:forEach>
            
                </tbody>
            </table>
        </div>-->
        <!-- este es para el chat -->
      <!--<div class="col-md-8">
            <textarea></textarea>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/chat" method="post" modelAttribute="chat">
            <div class="row">
                <div col="col-md-8">
                    <input type="text" name="msg"/>
                </div>
                <div class="col-md-4">
                <input type="submit" value="Enviar"></input>
                </div>
            </div>
    </form>-->


      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/usuariass/js/scripts.js"></script>
    <script src="${contextPath}/resources/js/comunication.js "></script>
</body>
</html>