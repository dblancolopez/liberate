package com.enfocat.liberate.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "DNI requerido")
    private String dni;

    private String foto;
    private String email;

    private int puntos;

    private String descripcion;

    @NotBlank(message = "Nombre requerido")
    private String nombre;

    @NotBlank(message = "Password requerido")
    private String Password;

    @NotBlank(message = "Rol requerido")
    private String rol;

    public Usuario() {
    }

    public Usuario(@NotBlank(message = "DNI requerido") String dni,
            @NotBlank(message = "Nombre requerido") String nombre,
            @NotBlank(message = "Password requerido") String password) {
        this.dni = dni;
        this.nombre = nombre;
        Password = password;
    }

    public Usuario(long id, @NotBlank(message = "DNI requerido") String dni, String foto, int puntos,
            String descripcion, @NotBlank(message = "Nombre requerido") String nombre,
            @NotBlank(message = "Password requerido") String password,
            @NotBlank(message = "Rol requerido") String rol,String email) {
        this.id = id;
        this.email = email;
        this.dni = dni;
        this.foto = foto;
        this.puntos = puntos;
        this.descripcion = descripcion;
        this.nombre = nombre;
        Password = password;
        this.rol = rol;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario [nombre=" + nombre + "]";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}