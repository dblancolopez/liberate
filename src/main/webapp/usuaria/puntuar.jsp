<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%


    /*boolean datosOk;

    if ("POST".equalsIgnoreCase(request.getMethod())) {
        
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dni = request.getParameter("dni");


        datosOk=true;
        if (datosOk){
            Contacto a = new Contacto(nombre, email, ciudad, telefono);
            ContactoController.save(a);

            response.sendRedirect("/contactos/contacto/list.jsp");
            return;
        }
    }

*/
%>


<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Aliberate</title>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/contactos/css/puntuacion.css">
</head>

<body>

  <%@include file="/menu.jsp"%>
  <%@include file="/panic.jsp"%>

  <div class="container-puntuar"> 
      <div class="row">
            <div class="col-md-12">
            <img class="img-thumbnail" src="../img/paquita.jpg" alt="../img/nofoto.png" />  
            <br> 
                <h3 style="text-align: center!important">Paquita</h3> 
                
                <form>
                    <p class="clasificacion" style="text-align: center!important">
                        <input id="radio1" type="radio" name="estrellas" value="5">
                            <label for="radio1"><i class="fas fa-star fa-2x"></i></label>
                        <input id="radio2" type="radio" name="estrellas" value="4">
                            <label for="radio2"><i class="fas fa-star fa-2x"></i></label>
                        <input id="radio3" type="radio" name="estrellas" value="3">
                            <label for="radio3"><i class="fas fa-star fa-2x"></i></label>
                        <input id="radio4" type="radio" name="estrellas" value="2">
                            <label for="radio4"><i class="fas fa-star fa-2x"></i></label>
                        <input id="radio5" type="radio" name="estrellas" value="1">
                            <label for="radio5"><i class="fas fa-star fa-2x"></i></label>
                    </p>
                </form>
                <button type="submit" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important"><strong>Validar</strong></button>
                           
            </div>
            
                 
        </div>   
    </div>




  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>

</body>

</html>