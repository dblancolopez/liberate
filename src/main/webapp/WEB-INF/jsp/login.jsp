<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <title>Log in </title>

      <!--<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href=".\resources\css\estilos.css">
</head>

<body>

<%@include file="panic.jsp"%>

  <nav class="navbar navbar-expand-lg  navbar-dark bg-primary" style="background-color: rebeccapurple!important">
    <a class="navbar-brand">
      <img src="./resources/img/logo.png" width="60" height="60" style="border-radius: 25%!important" border-radius="25%" class="d-inline-block align-top" href="/index.jsp" alt="logo">
    </a>
    <a class="navbar-brand" style="color: white">LIBERATE</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
      </div>
    <form method="POST" action="${contextPath}/login" class="form-signin"> 
      <div class="form-inline ${error != null ? 'has-error' : ''}">
        <span>${message}</span>
          <input name="username" type="text" class="form-control" placeholder="Usuaria" autofocus="true"/>
          <input name="password" type="password" class="form-control" placeholder="Password"/>
          <span>${error}</span>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
         
        <button class="btn btn-success btn-outline-dark" style="background-color: violet!important" type="submit">Log In</button>
        <a href="${contextPath}/registration" class="btn btn-success btn-outline-dark" style="background-color: violet!important">Crear</a>
      </div>
    </form>
  </nav>



<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="./resources/img/fondo.jpg" class="d-block w-100" alt="carousel1">
    </div>
    <div class="carousel-item">
      <img src="./resources/img/fondo1.jpg" class="d-block w-100" alt="carousel2">
    </div>
    <div class="carousel-item">
      <img src="./resources/img/fondo2.jpg" class="d-block w-100" alt="carousel3">
    </div>
  </div>
</div>
    <!--<div class="container">
      <form method="POST" action="${contextPath}/login" class="form-signin">
       

        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span>
            <input name="username" type="text" class="form-control" placeholder="Nombre de usuario"
                   autofocus="true"/>
            <input name="password" type="password" class="form-control" placeholder="Password"/>
            <span>${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
            <h4 class="text-center"><a href="${contextPath}/registration">Crear una cuenta</a></h4>
        </div>
      </form>
    </div>-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>








  
