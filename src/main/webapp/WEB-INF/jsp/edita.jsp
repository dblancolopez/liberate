<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@ page import = "org.springframework.validation.*" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>




<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Editar Perfil</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href=".\resources\css\estilos.css">
</head>

<body>

 <%@include file="navbar.jsp"%>
  <%@include file="panic.jsp"%>

  <div class="container-edita">
    <div class="row">
      <div class="col" style="margin-left: 50px!important">
        <h1>Editar Perfil de <c:out value= "${pageContext.request.userPrincipal.name}"/></h1>
      </div>
    </div>

    <form:form action="${pageContext.request.contextPath}/edita/${user.id}" method="post" modelAttribute="user">
      <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              <spring:message code="lbl.username" text="Nuevo Nombre" />
              <form:input path="username" class="form-control" />
              <form:errors path="username" cssClass="error" />
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
            <spring:message code="lbl.email" text="Nuevo Email" />
              <form:input path="email" class="form-control" />
              <form:errors path="email" cssClass="error" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
              <label>Contraseña</label>
                <input type="password" class="form-control" 
                  placeholder="Password">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Repetir Contraseña</label>
                <input type="password" class="form-control" 
                  placeholder="Repetir Password">
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
            <spring:message code="lbl.descripcion" text="Nuev Caso Personal" />
              <textarea name="descripcion" style="border-radius: 8px!important"rows="5" cols="68" placeholder="Escribe aquí una breve descripción de tu caso personal."></textarea>
              <form:errors path="descripcion" cssClass="error" />
            </div>
          </div>
        </div>
        <div class="col-md-4">
        <div>
          <img class="img-thumbnail" src="http://placekitten.com/300/300?image=${user.id}" alt="./resources/img/nofoto.png" />
          <form id="formcrea" action="/contactos/subefoto" method="POST" enctype="multipart/form-data">
          <a class="imagen"><button type="button" class="btn btn-primary"><i class="fas fa-camera"></i></button></a>
          </div>
        <br>
        <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                  <button type="submit" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important">Guardar</button>
                  </div>
                </div>
              <div class="col-md-6">
                <div class="form-group">
                  <button type="cancel" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important">Cancelar</button>
                </div>
              </div>
             <div class="col-md-12">
              <div class="form-group">
                <a href="#popup"><button type="button" class="btn btn-block btn-success btn-danger">Eliminar Cuenta</button></a>
                <div id="popup" class="overlay">
                <div class="container-elimina">
            <form id="formcrea" action="#" method="POST">
                <div class="row">
            <div class="col-md-8">
            <div class="row">
                <div class="col-md-8">
                <div class="form-group">
                    <label for="dniInput">Introduce tu DNI para confirmar darse de baja</label>
                    <input name="dni" type="text" class="form-control" id="nombreInput"
                    placeholder="DNI 00000000-X">
                </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <button type="submit" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important">Confirmar</button>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <button type="cancel" class="btn btn-block btn-success btn-outline-dark" style="background-color: violet!important">Cancelar</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
          <img class="img-thumbnail" src="./resources/img/logo.png" alt="./resources/img/nofoto.png" /> 
      </div>
       
      </div>
     </form:form>

  </div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>

</body>

</html>