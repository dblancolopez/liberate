<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Crear una cuenta</title>

      <!--<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./resources\css\estilos.css">
  </head>

  <body>


<div class="container-crea">
    <div class="row">
        <div class="col-md-12">
            <form:form method="POST" modelAttribute="userForm" class="form-signin">
            <h2>Crea tu cuenta</h2>
            <div class="row">
                <div class="col-md-6">
                        <spring:bind path="username">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                <label for="nombreInput">Nombre</label>
                                    <form:input type="text" path="username" class="form-control" placeholder="Insertar Nombre de usuaria"
                                                autofocus="true"></form:input>
                                    <form:errors path="username"></form:errors>
                                </div>
                        </spring:bind>
                    </div>
                    <div class="col-md-6">
                        <spring:bind path="email">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                <label for="emailInput">Email</label>
                                    <form:input type="text" path="email" class="form-control"  placeholder="Ejemplo usuaria@gmail.es"
                                                autofocus="true"></form:input>
                                    <form:errors path="email"></form:errors>
                                </div>
                        </spring:bind>
                    </div>
                </div>
            </div> 
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                <label for="passwordInput">Contraseña</label>
                    <spring:bind path="password">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
                        <form:errors path="password"></form:errors>
                    </div>
                </spring:bind>
                </div>
                <div class="col-md-6">
                <label for="confirmPasswordInput">Repite Contraseña</label>
                     <spring:bind path="passwordConfirm">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:input type="password" path="passwordConfirm" class="form-control"
                                    placeholder="Confirmar password"></form:input>
                        <form:errors path="passwordConfirm"></form:errors>
                    </div>
                </spring:bind>
                </div>
              </div> 
            </div>  
        
      
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                <spring:bind path="descripcion">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="descripcionInput">Caso Personal</label>
                                <form:textarea type="text" path="descripcion" class="form-control" style="resize:none;width:100%;height:100px" maxlength="255" placeholder="Escribe aquí una breve descripción de tu caso."
                                            autofocus="true"></form:textarea>
                                <form:errors path="descripcion"></form:errors>
                            </div>
                        </spring:bind>
                </div>
            </div>
         
        </div>
   <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                  <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                </div>
            </div>
         
        </div>
    </div>
  
    </form:form>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>
