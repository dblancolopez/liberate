package com.enfocat.liberate.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "chat")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long idUsuario;

    private long idConversacion;

    private String mensajes;

    private String sender;

    private Date data;

    private MessageType type;
    public enum MessageType{
        CHAT,
        JOIN,
        LEAVE
    }

    public Chat() {
    }

    public Chat(long idUsuario, long idConversacion, String mensajes, Date data) {
        this.idUsuario = idUsuario;
        this.idConversacion = idConversacion;
        this.mensajes = mensajes;
        this.data = data;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long isIdConversacion() {
        return idConversacion;
    }

    public void setIdConversacion(long idConversacion) {
        this.idConversacion = idConversacion;
    }

    public String getMensajes() {
        return mensajes;
    }

    public void setMensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

}