<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@ page import = "org.springframework.validation.*" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html>
<html lang="es-ES">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Perfil</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
      integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href=".\resources\css\estilos.css">
  </head>

  <body>

    <%@include file="navbar.jsp"%>
    <%@include file="panic.jsp"%>

    <div class="container-edita">
      <div class="row">
        <div class="col" style="margin-left: 50px!important">
          <h1>Perfil de <c:out value= "${pageContext.request.userPrincipal.name}"/></h1>
        </div>
      </div>
      <div class="row">
              <div class="col-md-4">
                  <img class="img-thumbnail" src="http://placekitten.com/300/300?image=${user.id}" alt="../img/nofoto.png" />    
                  <p>Puntuación <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                  <p>Rol: Mentora </p>
                  <a href="${pageContext.request.contextPath}/edita"><button type="button" class="btn btn-primary"><i class="fas fa-wrench fa-2x"></i></button></a>                
              </div>
              
              <div class="col-md-8">
                  <div class="row">
                      <form:form action="${pageContext.request.contextPath}/perfil/${user.id}" method="post" modelAttribute="user">
                      
                      <spring:message code="lbl.username" text="Nombre" />
                      <p><h5>${user.username}</h5></p>
                      <form:errors path="username" cssClass="error" />
                      <br>
                      <spring:message code="lbl.email" text="Email" />
                       <p><h5>${user.email}</h5></p>
                      <form:errors path="email" cssClass="error" />
                      <br>
                      <spring:message code="lbl.descripcion" text="Caso Personal" />
                      <p><h5>${user.descripcion}</h5></p>
                      <form:errors path="descripcion" cssClass="error" />
                      </form:form>
                  </div>
              </div> 
          </div>       
      </div>   
  </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
      integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
      integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
      crossorigin="anonymous"></script>

  </body>

</html>