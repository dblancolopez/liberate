package com.enfocat.liberate.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "mllegits")
public class MissatgesLlegits {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long idUsuario;
    private long idUsuarioMensaje;
    private long idConversacion;

    public MissatgesLlegits() {
    }

    public MissatgesLlegits(long idUsuario, long idUsuarioMensaje, long idConversacion) {
        this.idUsuario = idUsuario;
        this.idUsuarioMensaje = idUsuarioMensaje;
        this.idConversacion = idConversacion;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdUsuarioMensaje() {
        return idUsuarioMensaje;
    }

    public void setIdUsuarioMensaje(long idUsuarioMensaje) {
        this.idUsuarioMensaje = idUsuarioMensaje;
    }

    public long getIdConversacion() {
        return idConversacion;
    }

    public void setIdConversacion(long idConversacion) {
        this.idConversacion = idConversacion;
    }

}