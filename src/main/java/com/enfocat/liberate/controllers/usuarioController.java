package com.enfocat.liberate.controllers;

import javax.validation.Valid;

import com.enfocat.liberate.models.Usuario;
import com.enfocat.liberate.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(path="/usuario")
public class UsuarioController {
    
    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioController(UsuarioRepository usuarioRep) {
        this.usuarioRepository = usuarioRep;
    }
   
// @GetMapping("/addusuario")
// public String addusuario(Usuario usuario) {
//     return "add-usuario";
// }

// @RequestMapping(value = "/usuario/addusuario", method = RequestMethod.POST)
// public String processAdd(@Valid @ModelAttribute("usuario") Usuario usuario, BindingResult bindingResult) {
//     if (bindingResult.hasErrors()) {
//         return "add-usuario";
//     }

//     return "usuario-added-conexito";
// }
   
    @GetMapping("/all")
    public String getAll(Model model){
        model.addAttribute("usuarios", usuarioRepository.findAll());
        return "usuario-list";
    }

    @GetMapping("/addusuario")
    public String addUsuario(Usuario usuario) {
        return "add-usuario";
    }

    @PostMapping("/addusuario")
    public String addUsuario(@Valid Usuario usuario, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-usuario";
        }

        usuarioRepository.save(usuario);
        model.addAttribute("usuarios", usuarioRepository.findAll());
        return "add-usuario";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid usuario Id:" + id));
        model.addAttribute("usuario", usuario);
        return "update-usuario";
    }

    @PostMapping("/update/{id}")
    public String updateUsuario(@PathVariable("id") long id, @Valid Usuario usuario, BindingResult result, Model model) {
        if (result.hasErrors()) {
            usuario.setId(id);
            return "update-usuario";
        }
        
        usuarioRepository.save(usuario);
        model.addAttribute("usuarios", usuarioRepository.findAll());
        return "usuario-list";
    }

    @GetMapping("/delete/{id}")
    public String deleteUsuario(@PathVariable("id") long id, Model model) {
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid usuario Id:" + id));
        usuarioRepository.delete(usuario);
        model.addAttribute("usuarios", usuarioRepository.findAll());
        return "usuario-list";
    }
    

}