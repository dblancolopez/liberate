package com.enfocat.liberate.repositories;

import com.enfocat.liberate.models.Chat;


import org.springframework.data.repository.CrudRepository;

public interface ChatRepository extends CrudRepository<Chat, Long> {

}
