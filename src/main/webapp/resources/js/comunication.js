var stompClient = null;
var messageInput = document.querySelector('#textInput');
var messageArea  = document.querySelector('.campo');
var usernameForm  = document.querySelector('#usernameForm');
var messageForm  = document.querySelector('.chat');
/*function setConnected(connected)
{
    $(#)
}*/
var stompClient = null;
var username = null;
var socket;
var colors = ['#2196F3', '#32c787', '#00BCD4', '#ff5652',
              '#ffc107', '#ff85af', '#FF9800', '#39bbb0'];

function connect(event){
    //username = document.querySelector('#username').value.trim();
    username = document.getElementById("userHidden");
    console.log("usuari: "+username.value);
    if(username)
    {
         socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);
        //var stompClient = Stomp.client('ws://localhost:8080/chat');
        stompClient.connect({},onConnected,onError);
    }
    // event.preventDefault();
}

function onConnected(){
    //Subscripcion al Public Topic
    console.log("onconnected---------------");
    stompClient.subscribe('/topic/public',onMessageReceived);
    //Pasa el nombre del usuario al servidor
    stompClient.send("/app/chat.addUser",
                    {},
                    JSON.stringify({sender: username.value, type: 'JOIN'})
                    );
}

function onError(error){
    alert("No se ha podido conectar al servidor webSocket. Por favor vuelva a intentarlo.");
}
 

function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username.value,
            mensajes: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


/*function sendMessage(event){
    

    var messageContent = messageInput.value.trim();

    console.log("semdMessage "+messageContent);
    if(messageContent && stompClient)
    {
        var chatMessage ={
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };
        console.log("Estos son los elementos existentes: "+chatMessage.sender+", "+chatMessage.content+", "+", "+chatMessage.type)
        // stompClient.send("/app/chat",{},JSON.stringify({'name': $("#name").val()+": "+chatMessage}));
        stompClient.send("/app/chat.sendMessage",{},JSON.stringify(messageInput.value));
        messageInput.value= '';
    }
    event.preventDefault();
}*/
function onMessageReceived(payload){

    console.log("rebut", payload.body);
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN')
    {
        messageElement.classList.add('event-message');
        message.mensajes = message.sender + ' Se ha unido!';
    }else if(message.type === 'LEAVE')
        {
            console.log("Desconecta!!!!!");
            messageElement.classList.add('event-message');
            message.mensajes = message.sender + ' ha abandonado!';
            
        }else{
            messageElement.classList.add('chat-message');

            var avatarElement = document.createElement('i');

            var avatarText = document.createTextNode(message.sender[0]);
            avatarElement.appendChild(avatarText);
            avatarElement.style['background-color'] = getAvatarColor(message.sender);

            messageElement.appendChild(avatarElement);

            var usernameElement = document.createElement('span');
            var usernameText = document.createTextNode(message.sender);
            usernameElement.appendChild(usernameText);
            messageElement.appendChild(usernameElement);
        } 

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.mensajes);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

function getAvatarColor(messageSender)
{
    var hash = 0;
    for(var i = 0; i < messageSender.length;i++)
    {
        hash = 31* hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

//messageForm.addEventListener('submit',connect,true);
if(messageForm){
    messageForm.addEventListener('submit',sendMessage,true);
}


/*function showGreeting(message)
{
    $('#requestM').append(message);
}
$(function(){
   $("#enviar").click(function(){sendMessage();}); 
});*/
$(document).ready(function(){
    connect();
});


