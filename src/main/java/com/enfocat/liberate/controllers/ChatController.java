package com.enfocat.liberate.controllers;

import java.util.NoSuchElementException;
import java.util.Optional;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.enfocat.liberate.service.SecurityService;
import com.enfocat.liberate.service.SecurityServiceImpl;
import com.enfocat.liberate.service.UserService;
import com.enfocat.liberate.models.Chat;
import com.enfocat.liberate.models.User;
import com.enfocat.liberate.repositories.ChatRepository;
import com.enfocat.liberate.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;


@Controller
//@   RequestMapping(path="")
public class ChatController extends HttpServlet{
    
    /*@Autowired
    private SecurityService securityService;*/
    
    private UserRepository userRepo;
    private ChatRepository chatRepo;
    //private final UsuarioRepository UsuarioRepository;
    
    /*@   GetMapping("/all")
    public String getAll(Model model){
        model.addAttribute("chat", chatRepository.findAll());
        return "chat-list";
    }*/
    
    // @    GetMapping("/addmensaje/{idconversa}")
    // public String addMensaje(@PathVariable("idconversa") long idconversa, Chat chat, Model model) {
    //     model.addAttribute("idconversa", idconversa);
    //     return "add-mensaje";
    // }
    public ChatController(UserRepository userRepo, ChatRepository chatRepo)
    {
        this.userRepo = userRepo;
        this.chatRepo = chatRepo;
    }
    @GetMapping("/chat") 
    public String getAll(Model model){
        //model.addAttribute("chat", chatRepository.findAll());
        
        return "chat";
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public Chat sendMessage(@Payload Chat chat)
    {
        // User usuari = userRepo.findByUsername(chat.getSender());
         return chat;
    }


    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public Chat addUser(@Payload Chat chat, SimpMessageHeaderAccessor headerAccesor){

        // User usuari = userRepo.findByUsername(chat.getSender());
        // chat.setUser(usuari);
        // chatRepo.save(chat);
        headerAccesor.getSessionAttributes().put("usuari",chat.getSender());
        return chat;
    }
    
    
    
    
/*
    @PostMapping("/chat")
    public String addUsuario(@Valid Chat chat, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-mensaje";
        }

        chatRepository.save(chat);
        model.addAttribute("msg", chat.getMensajes());
        return "chat";
    }*/



    
}